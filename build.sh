echo "<link rel="stylesheet" href="styles/styles.css">" > index.html &&
echo "<link rel="stylesheet" href="styles/styles.css">" > contact.html &&
markdown page.md >> index.html
markdown contact.md >> contact.html

# BUILD THE GALLERY
echo "<!DOCTYPE html>
<html>
<head>
<style>
div.gallery {
  border: 1px solid #ccc;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 6px;
  float: left;
  width: 24.99999%;
}

@media only screen and (max-width: 1200px) {
  .responsive {
    width: 49.99999%;
    margin: 6px 0;
  }
}

@media only screen and (max-width: 800px) {
  .responsive {
    width: 100%;
  }
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>

<h2>Gallery</h2>

<h4>Welcome to my gallery</h4>" > gallery.html &&


for x in ./gallery/*avif
do
    x="${x:2}"
    echo "<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="$x">
      <img src="$x" alt="Cinque Terre" width="600" height="400">
    </a>">> gallery.html

  x=${x%.*}
  x=${x##*/}

  echo "    <div class="desc">$x</div>
  </div>
</div>" >> gallery.html
done

echo "<div class="clearfix"></div>

</body>
</html>" >> gallery.html &&

echo "DONE!"
