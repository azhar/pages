# Yo!

## My name is Azhar

![](img/moi.avif)

[Gallery](/gallery)

### Stuff I do
#### Tech
I build and fix computers, write programs,  
hardware hacking and mods, free(libre) software installation

#### Art
Digital art mostly pixel art  
Drawing regular art on paper  
Spray paint grafiti and space art  
Beatboxing  
Photography  
8-Bit music

#### Lighting
Colorful and ambiance LED installations  

---

# Contact info}
---

### JAMI

> 25aba1c52ddd7b1ed26bfdf71f160538a98e0fda

Username: azhar-  

---
### TOX

> 531EAE336E596F5330505E94041BD98B6B80A37C0D781AA84004216BEDFCE8453A8E1536DB31

Username: azhar

---
### Matrix
Coming soon

---
### XMPP
Coming soon

---
### email
Coming soon
